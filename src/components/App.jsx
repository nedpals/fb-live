/* eslint-disable */
/* global FB */

import React, { Component } from 'react';
import Header from './Header';
import LiveGenForm from './LiveGenForm';
import Footer from './Footer';
import '../styles/App.css';
import update from 'immutability-helper';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      auth: {
        userId: '', 
        accessToken: '', 
        loggedIn: false        
      },
      data: {
        title: 'Livestream',
        description: '',
        save_vod: true,
        privacy: { value: 'EVERYONE' }
      },
      result: undefined,
      streamDetails: undefined,
      options: {
        is_ssl: false,
        separate: false
      }
    };
    this.fbLogin = this.fbLogin.bind(this);
    this.submitDetails = this.submitDetails.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleOptions = this.handleOptions.bind(this);
  }

  submitDetails(e) {
    e.preventDefault();
    FB.api(`/${this.state.auth.userId}/live_videos`, 'post', this.state.data, (response) => {
      this.setState({ result: response });
      console.log(this.state.result);

      FB.api(`/${this.state.result.id}`, 'get', {fields: 'permalink_url'}, (response2) => {
        this.setState({ streamDetails: response2 });
        console.log(this.state.streamDetails);
      });
    });
  }

  handleChange(childName, childValue) {
     let newData = update(this.state, {data: {$merge: {[childName]: childValue}}});
     this.setState(newData);
  }

  handleOptions(e) {
    let target = e.target;
    let value = eval(`!this.state.options.${target.name}`);
    let name = target.name;
    let options = update(this.state, {options: {$merge: {[name]: value}}});
    this.setState(options);
    console.log(this.state.options)
  }

  componentDidMount() {
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '221049541695446',
        xfbml      : true,
        version    : 'v2.9'
      });
      FB.AppEvents.logPageView();
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  }

  fbLogin(e) {
    e.preventDefault();

    if (this.state.auth.loggedIn) {
      FB.logout(() => {
        this.setState({ auth: { userId: '', accessToken: '', loggedIn: false }, result: undefined, streamDetails: undefined });
      })
    } else {
      FB.login((response) => {
        if (response.status === "connected") {
          let ar = response.authResponse;
          this.setState({ auth: { userId: ar.userID, accessToken: ar.accessToken, loggedIn: true } });
        }
      },{scope: 'manage_pages,publish_actions'});
    }
  }

  render() {
    let loggedIn = this.state.auth.loggedIn;
    let appResult;

    if (loggedIn && this.state.result !== undefined && this.state.streamDetails !== undefined) {
      let options = this.state.options;
      let streamUrl = options.is_ssl ? this.state.result.secure_stream_url : this.state.result.stream_url;
      let streamUrl2, streamKey, urlCount, streamKeyBox;

      if (options.separate) {
        urlCount = options.is_ssl ? 39 : 37;
        streamUrl2 = streamUrl.slice(0, urlCount);
        streamKey = streamUrl.slice(urlCount);
        streamKeyBox = <div className="stream-url">
                          <p>{streamKey}</p>
                        </div>;
      }

      appResult = <div className="App-result">
                    <p>Congratulations! Copy it below:</p>
                    <div className="stream-url">
                      <p>{options.separate ? streamUrl2 : streamUrl}</p>
                    </div>
                    {streamKeyBox}
                    <input 
                      type="checkbox" 
                      name="is_ssl"
                      checked={this.state.done || this.props.done }  
                      onChange={ this.handleOptions } />
                    <label for="is_ssl">Use secure URL</label>
                    <input 
                      type="checkbox"
                      name="separate" 
                      checked={ options.separate } 
                      onChange={ this.handleOptions } />
                    <label for="separate">Split URL and stream key</label>
                    <p><i>You can view your video <a href={'http://facebook.com' + this.state.streamDetails.permalink_url}>here</a>.</i></p>                    
                  </div>;
    }

    return (
      <div className="App">
        <Header />
        {appResult}
        <div className="App-intro">
          <LiveGenForm loggedIn={loggedIn} handleChange={this.handleChange} submitDetails={this.submitDetails} data={this.state.data} />
          <button onClick={this.fbLogin}>{ loggedIn ? "Logout" : "Login with Facebook" }</button>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
